﻿using System;
using System.Collections.Generic;

namespace FilterByPalindromicTask
{
    public static class ArrayExtension
    {
        public static int[] FilterByPalindromic(int[] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Length == 0)
            {
                throw new ArgumentException("Source is empty.");
            }

            List<int> palindromicNumbers = new List<int>();

            for (int i = 0; i < source.Length; i++)
            {
                if (NumberIsPalindromic(source[i]))
                {
                    palindromicNumbers.Add(source[i]);
                }
            }

            return palindromicNumbers.ToArray();
        }

        public static bool NumberIsPalindromic(int number)
        {
            if (number < 0)
            {
                return false;
            }

            int numberSize = (number == 0) ? 1 : 0;
            int copyNumber = number;

            while (copyNumber != 0)
            {
                copyNumber /= 10;
                numberSize++;
            }

            int palindromSize = numberSize;

            for (int i = 0; i < numberSize; i++)
            {
                if (palindromSize <= 1)
                {
                    return true;
                }

                int lastValue = number % 10;
                int firstValue = (int)(number / Math.Pow(10, palindromSize - 1));

                if (firstValue == lastValue)
                {
                    number %= (int)Math.Pow(10, palindromSize - 1);
                    number /= 10;
                    palindromSize -= 2;
                    continue;
                }

                break;
            }

            return false;
        }
    }
}
